<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index')->with('users', User::paginate(8));
    }

    public function edit($id)
    {
        if (Auth::user()->id == $id) {
            return redirect(route('admin.users.index'))->with('danger', 'you are not able to change your role');
        }
        return view('admin.users.edit')->with(['user' => User::find($id), 'roles' => Role::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->id == $id) {
            return redirect(route('admin.users.index'))->with('danger', 'you are not able to change your role');
        }
        $user = User::find($id);
        $user->roles()->sync($request->roles);
        return redirect()->route('admin.users.index')->with('success', 'user has been updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->id == $id) {
            return redirect(route('admin.users.index'))->with('danger', 'you can not able to delete your self');
        }
        if ($user) {
            $user->roles()->detach();
            $user->delete();
            return redirect()->route('admin.usecrs.index')->with('success', 'user has been Deleted');
        }
        return redirect()->route('admin.users.index')->with('danger', 'user can not be  Deleted');
    }
}
